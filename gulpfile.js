var gulp            = require('gulp');
var postcss         = require('gulp-postcss');
var mixins          = require('postcss-mixins');
var postcssImport   = require('postcss-import');
var precss          = require('precss');
var autoprefixer    = require('autoprefixer');
var cssnano         = require('cssnano');

gulp.task('css:build', function () {
  return gulp
    .src('./src/style.css')
    .pipe(postcss([
      mixins(),
      postcssImport(),
      precss(),
      autoprefixer({browsers: ['last 1 version']}),
      cssnano(),
    ]))
    .pipe(gulp.dest('./dest'));
});

gulp.task('prod', gulp.parallel('css:build'));

gulp.task('dev', function() {
  gulp.watch('./src/**/*.css', ['css:build'])
})
